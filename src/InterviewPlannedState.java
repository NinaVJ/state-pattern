public class InterviewPlannedState implements ApplicantState{

    @Override
    public void next(Applicant applicant) {
        applicant.setState(new InterviewState());
    }

    @Override
    public void printState() {
        System.out.println("Yay they are interested and want to see me :)");
    }
}
