public class JobReceivedState implements ApplicantState {

    @Override
    public void next(Applicant applicant) {
        System.out.println("This applicant is in its last state.");
    }

    @Override
    public void printState() {
        System.out.println("Yay I got the job :D");
    }
}
