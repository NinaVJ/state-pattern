public interface ApplicantState {

    //Een klasse(state) is alleen een ApplicantState als die de volgende methodes implementeert:
    void next(Applicant applicant);
    void printState();

}
