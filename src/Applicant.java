public class Applicant {

    //status houdt zich altijd aan de afspraken die in de interface staan
    private ApplicantState state;

    //Met de constructor van deze klasse maak ik een nieuw object aan
    //default state zet ik op resumeSent
    public Applicant() {
        this.state = new ResumeSentState();
    }

    //methode om de state op te halen
    public ApplicantState getState() {
        return state;
    }

    /*Hiermee kan ik een state setten op dit object. En ik kan kiezen wat de status is.
    Ik hoef deze niet te setten, want ik heb een vaste previousState en nextState
    De setstate geeft je de mogelijkheid om een andere volgorde van States te kiezen, en dat wil ik niet*/
    void setState(ApplicantState state) { //package private. nu kunnen alleen states binnen package aanpassen
        this.state = state;
    }

    public void nextState() {
        state.next(this);
    }

    public void printState() {
        state.printState();
    }
}

