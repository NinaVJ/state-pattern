public class ResumeSentState implements ApplicantState {

        @Override
        public void next(Applicant applicant) {
            applicant.setState(new InterviewPlannedState());
        }

        @Override
        public void printState() {
            System.out.println("Resume sent, no interview planned yet.");
        }
}
