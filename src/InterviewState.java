public class InterviewState implements ApplicantState{

    @Override
    public void next(Applicant applicant) {
        applicant.setState(new JobReceivedState());
    }

    @Override
    public void printState() {
        System.out.println("Hi there I am very excited to be here");
    }

}
