public class Main {

    public static void main(String[] args) {

        Applicant applicant = new Applicant();
        applicant.printState();

        for (int i = 0; i < 3; i++) {
            applicant.nextState();
            applicant.printState();
        }
    }
}
